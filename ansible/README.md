## Ansible playbooks to configure and manage servers

This repository contains an ansible playbook with multiples tasks to configure and manage ubuntu servers.

It also contains an ansible host inside a docker container.
You can also use some virtual machines using vagrant (Vagrantfile with VirtualBox provider) for quick testing purposes.

## Ansible playbook:

### Bootstrap playbook:

- install latest python3;
- ensure openssh-server is installed;
- ensure 'wheel' group is created;
- allow 'wheel' group to have passwordless sudo;
- create an *ansible_user* (to keep log files);

### Main playbook:

##### Roles:

- common setup for servers
    - create *admin_user*;
    - create *deployer_user*;
    - upgrade system with latest updates;
    - install some prerequisites packages;
    - remove packages that are no longer required;
    - limit SSH multiple attempts connections;
    - allow all access to TCP port 80 (HTTP);
    - allow all access to TCP port 443 (HTTPS);
    - enable UFW;
    - enable UFW logging;
    - install auditd package;
    - copy docker audit rules file into place;
    - copy docker deamon json file into place;
- devsec.hardening.ssh_hardening
- devsec.hardening.os_hardening
- oefenweb.fail2ban
- geerlingguy.swap
- geerlingguy.docker

## Quickstart

You can test every script against a ready Vagrant VM included in this repository.

First, install [Virtual Box](https://www.virtualbox.org/) and [Vagrant](https://www.vagrantup.com/downloads) on your machine:

Next, replace `ip: "xxx.xxx.xxx.xxx"` in `inventories/staging.yml` with any IP address inside your local network inventory file.

**TIP**: Use the `ip addr` command on terminal to list your current IP address and your network interfaces:

```yml
staging:
  children:
    vagrant:
      hosts:
        node-1:
          ansible_host: xxx.xxx.xxx.xxx # example: 10.0.0.150
                    ...
```

Now, include the **same** IP address used before and your host network adapter inside the `tfg/vagrant/ubuntu/focal64/virtualbox/Vagrantfile` file.

```ruby
config.vm.network "public_network", ip: "xxx.xxx.xxx.xxx", bridge: [ # same IP address
    "your-adapter-here" # example: "eno1" (onboard interface ethernet port 1) or
                        #          "wlp2s0" (wifi pci-slot=2 card-index=0)
]
```

Take a look at the [Consistent Network Device Naming](https://en.wikipedia.org/wiki/Consistent_Network_Device_Naming) convention used in Linux if you want to know more.

Use the following command to start the ansible container and the vagrant VM:

```
make start
```

After the VM initialization, you will be inside the ansible container automatically.

At last, test the connection with your VM using SSH password authentication:

```
ansible \
    -vv \
    --inventory inventories/staging.yml \
    --extra-vars "variable_ssh_user=vagrant" \
    --ask-pass \
    --module-name \
    ping \
        all
```

**Congratulations! If everything worked correctly, you can run this playbook against the VM.**

First, execute the `bootstrap.yml` playbook against your VM to install the basic Ansible requisites (Python 3.x) and to create the Ansible user responsible for managing your host:

```
ansible-playbook \
    -vv \
    --inventory inventories/staging.yml \
    --extra-vars "variable_ssh_user=vagrant" \
    --ask-pass \
    bootstrap.yml
```

Finally, run the `playbook.yml` playbook to execute every task and roles described in this repository:

```
ansible-playbook \
    -vv \
    --inventory inventories/staging.yml \
    playbook.yml
```

You can also execute single roles/tasks using tags:

```
ansible-playbook \
    -vv \
    --tags common \
    --inventory inventories/staging.yml \
    playbook.yml
```

## Setup with your own configs

To use this playbook against your server, follow the instructions below.

First you will have to add your managed node to the `inventories/production.yml` file.

```yml
production:
    children:
        hosts:
            # modify here
            aws_vps1:
                ansible_host: xxx.xxx.xxx.xxx # your server IP address
                ansible_user: "{{ variable_ssh_user | default(ansible_user_name) }}" # ansible remote user
                ansible_python_interpreter: /usr/bin/python3 # forcing python 3
```

Next, you must create your vault file in `inventories/group_vars/production/vault/common.yml`, containing password and the SHA salt for unique encrypting for the *admin_user* and *deployer_user* users. 

Use the command below to start an ansible container using docker in interactive mode:

```
make exec
```

Now, use the following command to create a new vault file:

```
ansible-vault \
    create \
        inventories/group_vars/production/vault/common.yml
```

Currently used `common.yml` template file:

```yml
---
# admin user password and salt
vault_admin_user_password_value: 'yourPassword' # change here
vault_admin_user_password_salt: 'yourPasswordSalt' # change this value too

# deployer user password and salt
vault_deployer_user_password_value: 'yourPassword' # change here
vault_deployer_user_password_salt: 'yourPasswordSalt' # change this value too
```

Then, you will also need to add the following execute the `inventories/group_vars/production/vault/common.yml`:

```yml
---
# admin user password and salt
admin_user_password_value: "{{ vault_admin_user_password_value }}"
admin_user_password_salt: "{{ vault_admin_user_password_salt }}"

# deployer user password and salt
deployer_user_password_value: "{{ vault_deployer_user_password_value }}"
deployer_user_password_salt: "{{ vault_deployer_user_password_salt }}"
```

Doing this way you can fork this code and push everything to a remote repository without worrying about sensitive variables.

Now, execute the `bootstrap.yml` playbook against your server to install the basic Ansible requisites (Python 3.x) and to create the Ansible user responsible for managing your host, by providing a password SSH access with the `--ask-pass`, `--ask-vault-pass` and `--ask-become-pass` (optionak: if your user has a sudo password) flag:

```
ansible-playbook \
    -vv \
    --inventory inventories/production.yml \
    --extra-vars "variable_ssh_user=your_user" \
    --ask-pass \
    --ask-vault-pass \
    --ask-become-pass \
    bootstrap.yml
```

**And finally, now you can run our main playbook against your host using the ansible user created from the bootstrap.yml playbook earlier:**

```
ansible-playbook \
    -vv \
    --invetory inventories/production.yml \
    --ask-vault-pass \
    playbook.yml
```

## References:

- [A collaborative curated list of awesome Ansible resources](https://github.com/jdauphant/awesome-ansible)
- [Ansible Collection - devsec.hardening](https://github.com/dev-sec/ansible-collection-hardening)
- [Ansible action plugin to merge variables](https://github.com/leapfrogonline/ansible-merge-vars): An Ansible plugin to merge all variables in context with a certain suffix (lists or dicts only) and create a new variable that contains the result of this merge. This is an Ansible action plugin, which is basically an Ansible module that runs on the machine running Ansible rather than on the host that Ansible is provisioning.
- [Ansible for DevOps Examples - Book from Jeff Geerling](https://github.com/geerlingguy/ansible-for-devops): This repository contains Ansible examples developed to support different sections of Ansible for DevOps, a book on Ansible by Jeff Geerling.
- [Ansible docs - strategy free](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/free_strategy.html#free-strategy): With the free strategy, unlike the default linear strategy, a host that is slow or stuck on a specific task won’t hold up the rest of the hosts and tasks.
- [Ansible docs - forks](https://docs.ansible.com/ansible/latest/user_guide/playbooks_strategies.html#setting-the-number-of-forks): By default, Ansible runs each task on all hosts affected by a play before starting the next task on any host, using 5 forks. If you have the processing power available and want to use more forks, you can set the number in ansible.cfg.
- [Ansible docs - using check_mode and diff](https://docs.ansible.com/ansible/latest/user_guide/playbooks_checkmode.html): Ansible provides two modes of execution that validate tasks: check mode and diff mode. These modes can be used separately or together. They are useful when you are creating or editing a playbook or role and you want to know what it will do. In check mode, Ansible runs without making any changes on remote systems. Modules that support check mode report the changes they would have made. Modules that do not support check mode report nothing and do nothing. In diff mode, Ansible provides before-and-after comparisons. Modules that support diff mode display detailed information. You can combine check mode and diff mode for detailed validation of your playbook or role.
- [YAML multiline without \n or spaces caracters](https://stackoverflow.com/questions/6268391/is-there-a-way-to-represent-a-long-string-that-doesnt-have-any-whitespace-on-mul)
- [Ansible CLI override inventory file](https://stackoverflow.com/a/33234120)
- [How to use SSH private keys for dynamic inventory files](https://stackoverflow.com/questions/33795607/how-to-define-ssh-private-key-for-servers-fetched-by-dynamic-inventory-in-files)

## TODO

- test paramiko with ufw not limiting ssh attempts (with ufw ssh limit task disabled);
- ports related to docker swarm should be opened only for the docker swarm advertised network; how da hell im supposed to do that? terraform? ansible? or leave open for now?
- fix openssh connection failing and have to resort to paramiko;
- add ufw docker fix:
    - need to find a suitable fix first;
    - maybe start using iptables directly;
        - needs a lot of testing!
- fix roles/common/handler for docker service;
    - add when statement (if docker is installed then execute handler);
    - same handler also failed on geerlingguy/docker role (different problem - timeout);
- replace normal vars with deeply-nested dict: [Changing a deeply-nested dict variable in an Ansible playbook](https://www.jeffgeerling.com/blog/2017/changing-deeply-nested-dict-variable-ansible-playbook)