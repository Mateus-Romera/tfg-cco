---
- name: Make sure we have a admin user group
  become: yes
  ansible.builtin.group:
    name: "{{ admin_user_group_name }}"
    gid: "{{ admin_user_group_gid }}"
    state: present

- name: Create admin user with root privileges
  become: yes
  ansible.builtin.user:
    name: "{{ admin_user_name }}"
    password: "{{ admin_user_password_value | password_hash('sha512', admin_user_password_salt) }}"
    state: present
    uid: "{{ admin_user_uid }}"
    group: "{{ admin_user_group_name }}"
    groups: [ 'sudo' ]
    append: yes
    create_home: yes
    shell: /bin/bash
    update_password: on_create
  no_log: yes

# This is just to automate our example
# Users should probably supply you with their public keys instead
- name: "Create SSH pair keys on ansible localhost container for admin user"
  community.crypto.openssh_keypair:
    path: "{{ lookup('env','HOME') + '/.ssh/' + admin_user_name + '_id_ed25519' }}"
    type: ed25519
    comment: "{{ admin_user_name + ' ssh key' }}"
    state: present
  delegate_to: localhost
  run_once: true

- name: "Set authorized SSH keys for admin user"
  become: yes
  ansible.posix.authorized_key:
    user: "{{ admin_user_name }}"
    key: "{{ item }}"
    state: present
  with_items:
    - "{{ lookup('file', lookup('env','HOME') + '/.ssh/' + admin_user_name + '_id_ed25519.pub') }}"

- name: Make sure we have a deployer user group
  become: yes
  ansible.builtin.group:
    name: "{{ deployer_user_group_name }}"
    gid: "{{ deployer_user_group_gid }}"
    state: present

- name: Create deployer user with docker privileges
  become: yes
  ansible.builtin.user:
    name: "{{ deployer_user_name }}"
    password: "{{ deployer_user_password_value | password_hash('sha512', deployer_user_password_salt) }}"
    state: present
    uid: "{{ deployer_user_uid }}"
    group: "{{ deployer_user_group_name }}"
    append: yes
    create_home: yes
    shell: /bin/bash
    update_password: on_create
  no_log: yes

# This is just to automate our example
# Users should probably supply you with their public keys instead
- name: "Create SSH pair keys on ansible localhost container for deployer user"
  community.crypto.openssh_keypair:
    path: "{{ lookup('env','HOME') + '/.ssh/' + deployer_user_name + '_id_ed25519' }}"
    type: ed25519
    comment: "{{ deployer_user_name + ' ssh key' }}"
    state: present
  delegate_to: localhost
  run_once: true

- name: "Set authorized SSH keys for deployer user"
  become: yes
  ansible.posix.authorized_key:
    user: "{{ deployer_user_name }}"
    key: "{{ item }}"
    state: present
  with_items:
    - "{{ lookup('file', lookup('env','HOME') + '/.ssh/' + deployer_user_name + '_id_ed25519.pub') }}"

- name: Upgrade system with latest updates
  become: yes
  ansible.builtin.apt:
    upgrade: "yes"
    update_cache: yes
    cache_valid_time: 86400 #One day

# - name: Upgrade system with latest updates
#   become: yes
#   ansible.builtin.apt:
#     upgrade: "yes"
#     update_cache: yes
#     cache_valid_time: 86400 #One day
#     # Waiting for allow-downgrade PR
#     # For now, using 'force: yes' is a shity solution
#     # - https://github.com/ansible/ansible/issues/29451
#     # broke on ansible 2.10
#     # force: yes
#     # broke on ansible 2.10
#     # dpkg_options: 'force-downgrade'
#   when:
#     - "'staging' in group_names"

- name: Install prerequisites
  become: yes
  ansible.builtin.apt:
    name: [ 'aptitude', 'python3-psutil', 'wget', 'nano', 'make', 'iputils-ping', 'ufw' ]
    state: present

- name: Remove dependencies that are no longer required
  become: yes
  ansible.builtin.apt:
    autoremove: yes

# ufw supports connection rate limiting, which is useful for protecting
# against brute-force login attacks. ufw will deny connections if an IP
# address has attempted to initiate 6 or more connections in the last
# 30 seconds. See  http://www.debian-administration.org/articles/187
# for details. Typical usage is:
# NEED TO TEST THIS USING PARAMIKO!!
# - name: Limit SSH multiple attempts connections
#   become: yes
#   community.general.ufw:
#     rule: limit
#     port: ssh
#     proto: tcp
- name: Allow SSH multiple attempts connections
  become: yes
  community.general.ufw:
    rule: allow
    port: ssh
    proto: tcp

- name: Allow all access to TCP port 80 (HTTP)
  become: yes
  community.general.ufw:
    rule: allow
    port: '80'
    proto: tcp

- name: Allow all access to TCP port 443 (HTTPS)
  become: yes
  community.general.ufw:
    rule: allow
    port: '443'
    proto: tcp

# Ports related to docker swarm should be opened only for the docker
# swarm advertised network
- name: Allow all access to TCP port 2377 (docker swarm mode)
  become: yes
  community.general.ufw:
    rule: allow
    port: '2377'
    proto: tcp

- name: Allow all access to TCP and UDP port 7946 (docker swarm mode)
  become: yes
  community.general.ufw:
    rule: allow
    port: '7946'

- name: Allow all access to UDP port 4789 (docker swarm mode)
  become: yes
  community.general.ufw:
    rule: allow
    port: '4789'
    proto: udp

- name: Enable UFW
  become: yes
  community.general.ufw:
    state: enabled

- name: Enable UFW logging
  become: yes
  community.general.ufw:
    logging: 'on'

- name: Install auditd package
  become: yes
  ansible.builtin.apt:
    name: [ 'auditd' ]
    state: present

- name: Copy docker audit rules file into place
  become: yes
  ansible.builtin.copy:
    src: docker.rules
    dest: /etc/audit/rules.d/
    owner: root
    group: root
    mode: '0600'
    backup: yes
  notify:
    - "restart auditd"

- name: Copy docker daemon json file into place
  become: yes
  ansible.builtin.copy:
    src: daemon.json
    dest: /etc/docker/
    owner: root
    group: root
    mode: '0600'
    backup: yes
  # Notify geerlingguy.docker role "restart docker" handler
  notify:
    - "restart docker"