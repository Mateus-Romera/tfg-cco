# Undergraduate Thesis (Trabalho Final de Graduação - TFG)

Título: Uso de ferramentas de Infraestrutura como Código e um Orquestrador de Linux contêineres para automatizar e facilitar a implantação de softwares em ambiente de produção em uma arquitetura IaaS
Title: Use of tools from Infrastructure as Code and a Linux Container Orchestrator to automate and facilitate the deployment of software in a production environment in an IaaS architecture

Author: [Mateus Villar](https://mateusvillar.com)

Graduation: [Computer Science](https://unifei.edu.br/prg/cursos-presenciais/itajuba-campus-sede/ciencia-da-computacao/)

Universidade Federal de Itajubá - [UNIFEI](https://unifei.edu.br/)

## References:

### Vagrant

- [Vagrant create multiple VMs loop](https://www.devopsroles.com/vagrant-create-multiple-vms/)